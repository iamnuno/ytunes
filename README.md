# yTunes

Java Spring application "cloning" iTunes.

There's two main features in this web application: 
- a front end written with template engine Thymeleaf, providing a simple home page randomly display 5 songs, artists and musical genres, as well as, a search bar that allows to search for songs in the database
- a RESTful API that allows for querying all customers, highest spenders, total customers in countries, specific customer's popular genres, adding a new customer to the database and updating an existing customer

## Deployment

The application was containerized with Docker and deployed to Heroku: https://ytunes-app.herokuapp.com/
NB: Heroku's dyno sleeps after 30 minutes of inactivity, so the first visit/request might take slightly longer than subsequent accesses/requests.

## Postman

A collection of HTTP requests is available in 'src/test/postman' to test the API. 
This should also give you a basic overview of the existing endpoints.
