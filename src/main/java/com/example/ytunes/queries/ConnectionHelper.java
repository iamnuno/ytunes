package com.example.ytunes.queries;

/**
 * Class with the connection url for database connections.
 */
public class ConnectionHelper {
    public static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
}
