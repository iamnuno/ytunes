package com.example.ytunes.queries;

import com.example.ytunes.models.Genre;
import com.example.ytunes.services.Logger;
import com.example.ytunes.services.RandomGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles database queries related to genre object.
 */
public class GenreRepository {

    private static final String URL = ConnectionHelper.CONNECTION_URL;
    private static Connection connection = null;

    /**
     * Returns list with genre' ids.
     * Used as a helper method to later get random ids.
     */
    private static List<Integer> getGenreIds() {
        List<Integer> genreIds = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT GenreId FROM Genre ORDER BY GenreId DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genreIds.add(resultSet.getInt(1));
            }

            Logger.log("Query 'get genre ids' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return genreIds;
    }

    /**
     * Returns list with 5 random genres.
     */
    public static List<Genre> getRandomGenres() {
        List<Genre> genres = new ArrayList<>();
        List<Integer> randomGenresIds = RandomGenerator.getRandoms(getGenreIds(), 5);

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT GenreId, Name FROM Genre WHERE GenreID = ? OR GenreID = ? OR GenreID = ? OR GenreID = ? OR GenreID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, randomGenresIds.get(0));
            preparedStatement.setInt(2, randomGenresIds.get(1));
            preparedStatement.setInt(3, randomGenresIds.get(2));
            preparedStatement.setInt(4, randomGenresIds.get(3));
            preparedStatement.setInt(5, randomGenresIds.get(4));
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(new Genre(resultSet.getInt(1), resultSet.getString(2)));
            }

            Logger.log("Query 'get random genres' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return genres;
    }
}