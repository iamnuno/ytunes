package com.example.ytunes.queries;

import com.example.ytunes.models.Artist;
import com.example.ytunes.services.Logger;
import com.example.ytunes.services.RandomGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles database queries related to Artist object.
 */
public class ArtistRepository {

    private static final String URL = ConnectionHelper.CONNECTION_URL;
    private static Connection connection = null;

    /**
     * Returns list with artists' ids.
     * Used as a helper method to later get random ids.
     */
    private static List<Integer> getArtistsIds() {
        List<Integer> artistsIds = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT ArtistId FROM Artist ORDER BY ArtistId DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artistsIds.add(resultSet.getInt(1));
            }

            Logger.log("Query 'get artists ids' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return artistsIds;
    }

    /**
     * Returns list with 5 random artists.
     */
    public static List<Artist> getRandomArtists() {
        List<Artist> artists = new ArrayList<>();
        List<Integer> randomArtistsIds = RandomGenerator.getRandoms(getArtistsIds(), 5);

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT ArtistId, Name FROM Artist WHERE ArtistID = ? OR ArtistID = ? OR ArtistID = ? OR ArtistID = ? OR ArtistID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, randomArtistsIds.get(0));
            preparedStatement.setInt(2, randomArtistsIds.get(1));
            preparedStatement.setInt(3, randomArtistsIds.get(2));
            preparedStatement.setInt(4, randomArtistsIds.get(3));
            preparedStatement.setInt(5, randomArtistsIds.get(4));
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artists.add(new Artist(resultSet.getInt(1), resultSet.getString(2)));
            }

            Logger.log("Query 'get random artists' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return artists;
    }
}
