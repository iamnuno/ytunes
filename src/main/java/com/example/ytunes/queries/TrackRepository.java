package com.example.ytunes.queries;

import com.example.ytunes.models.Genre;
import com.example.ytunes.models.Track;
import com.example.ytunes.services.Logger;
import com.example.ytunes.services.RandomGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles database queries related to track object.
 */
public class TrackRepository {

    private static final String URL = ConnectionHelper.CONNECTION_URL;
    private static Connection connection = null;

    /**
     * Returns list with tracks' ids.
     * Used as a helper method to later get random ids.
     */
    private static List<Integer> getTrackIds() {
        List<Integer> trackIds = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT TrackId FROM Track ORDER BY TrackId DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                trackIds.add(resultSet.getInt(1));
            }

            Logger.log("Query 'get track ids' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return trackIds;
    }

    /**
     * Returns list with 5 random tracks.
     */
    public static List<Track> getRandomTracks() {
        List<Track> tracks = new ArrayList<>();
        List<Integer> randomTrackIds = RandomGenerator.getRandoms(getTrackIds(), 5);

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT TrackId, Name FROM Track WHERE TrackID = ? OR TrackID = ? OR TrackID = ? OR TrackID = ? OR TrackID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, randomTrackIds.get(0));
            preparedStatement.setInt(2, randomTrackIds.get(1));
            preparedStatement.setInt(3, randomTrackIds.get(2));
            preparedStatement.setInt(4, randomTrackIds.get(3));
            preparedStatement.setInt(5, randomTrackIds.get(4));
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(resultSet.getInt(1), resultSet.getString(2)));
            }

            Logger.log("Query 'get random tracks' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return tracks;
    }

    public static List<Track> searchTracks(String searchValue) {
        List<Track> tracks = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT T.TrackId, T.Name, AL.Title, AR.Name, G.Name\n" +
                    "from Track T\n" +
                    "         JOIN Album AL ON AL.AlbumId = T.AlbumId\n" +
                    "         JOIN Artist AR ON AR.ArtistId = AL.ArtistId\n" +
                    "         JOIN Genre G ON G.GenreId = T.GenreId\n" +
                    "WHERE UPPER(T.Name) LIKE ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, "%" + searchValue.toUpperCase() + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5)
                ));
            }

            Logger.log("Query 'search tracks' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return tracks;
    }
}
