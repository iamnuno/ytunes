package com.example.ytunes.queries;

import com.example.ytunes.models.Country;
import com.example.ytunes.models.Customer;
import com.example.ytunes.models.PopularGenre;
import com.example.ytunes.models.Spender;
import com.example.ytunes.services.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles database queries related to Customer object.
 */
public class CustomerRepository {
    private static final String URL = ConnectionHelper.CONNECTION_URL;
    private static Connection connection = null;

    /**
     * Returns list of all customers.
     */
    public static List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7)
                ));
            }

            Logger.log("Query 'get customers' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return customers;
    }

    /**
     * Adds customer to database.
     * Returns true if customer was added to database and false otherwise.
     */
    public static boolean addCustomer(Customer customer) {
        boolean success = false;

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "INSERT INTO Customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            Logger.log("Query 'add customer' ran successfully");

        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            }
            catch (Exception exception){
                Logger.log(exception.toString());
            }
        }

        return success;
    }

    /**
     * Updates customer in database.
     * Returns true if customer was updated successfully and false otherwise.
     */
    public static boolean updateCustomer(Customer customer) {
        boolean success = false;

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "UPDATE Customer set FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=? where CustomerId=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getCustomerId());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            Logger.log("Query 'update customer' ran successfully");

        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            }
            catch (Exception exception){
                Logger.log(exception.toString());
            }
        }

        return success;
    }

    /**
     * Returns list of countries with total number of customers (per country) in descending order.
     */
    public static List<Country> getNumberOfCustomersInCountries() {
        List<Country> countries = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT Country, COUNT(CustomerId) FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countries.add(new Country(
                        resultSet.getString(1),
                        Integer.parseInt(resultSet.getString(2))
                ));
            }

            Logger.log("Query 'get number of customers in countries' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return countries;
    }

    /**
     * Returns list of spenders in descending order.
     * Spender extends Customer with the additional total spent in purchases.
     */
    public static List<Spender> getHighestSpenders() {
        List<Spender> spenders = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT C.CustomerId, C.FirstName, C.LastName, C.Country, C.PostalCode, C.Phone, C.Email, SUM(I.Total) FROM Customer C JOIN Invoice I on C.CustomerId = I.CustomerId GROUP BY C.CustomerId ORDER BY SUM(I.Total) DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spenders.add(new Spender(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getBigDecimal(8)
                ));
            }

            Logger.log("Query 'get highest spenders' ran successfully");
        } catch (SQLException exception) {
            Logger.log(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return spenders;
    }

    /**
     * Given a customer id returns the customer's object and a list of the popular musical genres for given customer
     */
    public static PopularGenre getCustomerPopularGenre(int customerId) {
        PopularGenre popularGenre = new PopularGenre();
        List<String> genres = new ArrayList<>();
        Customer customer = null;

        try {
            connection = DriverManager.getConnection(URL);
            Logger.log("Connection to database established");

            String sqlQuery = "SELECT count(G.GenreId) as Count,\n" +
                    "       G.Name           as Genre,\n" +
                    "       C.FirstName      AS FirstName,\n" +
                    "       C.LastName       AS LastName,\n" +
                    "       C.Country        AS Country,\n" +
                    "       C.Email          AS Email,\n" +
                    "       C.Phone          AS Phone,\n" +
                    "       C.PostalCode     AS PostalCode\n" +
                    "FROM Invoice I\n" +
                    "         JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId\n" +
                    "         JOIN Track T on IL.TrackId = T.TrackId\n" +
                    "         JOIN Genre G on T.GenreId = G.GenreId\n" +
                    "         JOIN Customer C on C.CustomerId = I.CustomerId\n" +
                    "WHERE I.CustomerId = ?\n" +
                    "GROUP BY G.GenreId\n" +
                    "ORDER BY count(G.GenreId)\n" +
                    "    DESC";

            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            boolean isCustomerAdded = false;
            int max = 0; // keep track of maximum genre purchases
            while (resultSet.next()) {
                if (!isCustomerAdded) {
                    customer = new Customer(
                            customerId,
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8));
                    popularGenre.setCustomer(customer);
                    isCustomerAdded = true;
                }
                if (resultSet.getInt(1) >= max) {
                    genres.add(resultSet.getString(2));
                    max = resultSet.getInt(1);
                } else { // if not >= no need to continue looping since the result set is in descending order
                    break;
                }
            }
            popularGenre.setGenres(genres);

            Logger.log("Query 'get customer's popular genres' ran successfully");
        } catch (SQLException exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                connection.close();
                Logger.log("Database connection closed");
            } catch (SQLException exception) {
                Logger.log(exception.toString());
            }
        }

        return popularGenre;
    }
}
