package com.example.ytunes.models;

/**
 * Model class for tracks.
 */
public class Track {
    private int trackId;
    private String trackName;
    private String albumName;
    private String artistName;
    private String genreName;

    public Track() {
    }

    public Track(int trackId, String trackName) {
        this.trackId = trackId;
        this.trackName = trackName;
    }

    public Track(int trackId, String trackName, String albumName, String artistName, String genreName) {
        this.trackId = trackId;
        this.trackName = trackName;
        this.albumName = albumName;
        this.artistName = artistName;
        this.genreName = genreName;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}