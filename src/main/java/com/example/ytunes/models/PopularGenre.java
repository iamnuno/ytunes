package com.example.ytunes.models;

import java.util.List;

/**
 * Model class for popular genres.
 * Needed for response objects related to API calls to /api/customers/customerId}/popular/genres.
 */
public class PopularGenre {
    private Customer customer;
    private List<String> genres;

    public PopularGenre() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }
}
