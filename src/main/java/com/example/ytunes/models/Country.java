package com.example.ytunes.models;

/**
 * Models class for country objects.
 * Needed for response objects related to API calls to /api/customers/countries.
 */
public class Country {
    private String countryName;
    private int numberOfCustomers;

    public Country(String countryName, int numberOfCustomers) {
        this.countryName = countryName;
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }
}
