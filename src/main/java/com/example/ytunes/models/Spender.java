package com.example.ytunes.models;

import java.math.BigDecimal;

/**
 * Model class for spenders.
 * Extends customer to add total spent by respective customer.
 * Needed for response objects related to API calls to /api/customers/spenders.
 */
public class Spender extends Customer {
    private BigDecimal totalSpent;

    public Spender(int customerId, String firstName, String lastName, String country,
                   String postalCode, String phoneNumber, String email, BigDecimal totalSpent) {
        super(customerId, firstName, lastName, country, postalCode, phoneNumber, email);
        this.totalSpent = totalSpent;
    }

    public BigDecimal getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(BigDecimal totalSpent) {
        this.totalSpent = totalSpent;
    }
}
