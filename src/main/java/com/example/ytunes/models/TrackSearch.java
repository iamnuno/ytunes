package com.example.ytunes.models;

import java.util.List;

/**
 * Model class for track search.
 * Used on UI for track searches.
 */
public class TrackSearch {
    private String searchValue;
    private List<Track> tracks;

    public TrackSearch() {
    }

    public TrackSearch(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }
}
