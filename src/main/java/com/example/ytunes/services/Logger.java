package com.example.ytunes.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Helper class to log information to console regarding database requests.
 */
public class Logger {
    public static void log(String message) {
        System.out.printf("%s :: %s%n", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()), message);
    }
}
