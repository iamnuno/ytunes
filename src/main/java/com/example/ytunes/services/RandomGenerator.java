package com.example.ytunes.services;

import java.util.*;

/**
 * Helper class to generate 5 random ints from a given list.
 * List should be in descending order.
 */
public class RandomGenerator {

    public static List<Integer> getRandoms(List<Integer> list, int total) {
        List<Integer> randomList = new ArrayList<>();
        Random random = new Random();
        while (randomList.size() < total) {
            int min = 1;
            int max = list.get(0); // list should be in descending order
            int randomInt = random.nextInt((max - min) + 1) + min;

            if (list.contains(randomInt) && !randomList.contains(randomInt)) {
                int index = list.indexOf(randomInt);
                randomList.add(list.get(index));
            }
        }

        return randomList;
    }
}
