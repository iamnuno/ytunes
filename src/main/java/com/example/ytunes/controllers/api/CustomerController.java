package com.example.ytunes.controllers.api;

import com.example.ytunes.models.PopularGenre;
import com.example.ytunes.models.Spender;
import com.example.ytunes.queries.CustomerRepository;
import com.example.ytunes.models.Country;
import com.example.ytunes.models.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class that handles API requests related to customer(s).
 * Database queries are performed by calls to the Customer Repository class.
 */
@RestController
public class CustomerController {

    /**
     * Returns all customers in database.
     * Database query is handled by calling Customer Repository.
     *
     * @return Response Entity object with a list of all customers
     */
    @GetMapping("/api/customers")
    public ResponseEntity<List<Customer>> getCustomers() {
        return ResponseEntity.status(HttpStatus.OK).body(CustomerRepository.getCustomers());
    }

    /**
     * Mapping for creating a new customer via POST.
     *
     * @param customer object to be created
     * @return Response Entity object with either 'CREATED' and Customer object or 'BAD REQUEST' HTTP code
     */
    @PostMapping("api/customers")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer) {
        if (CustomerRepository.addCustomer(customer)) {
            return ResponseEntity.status(HttpStatus.CREATED).body(customer);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Mapping for updating an already existing customer based on customer id.
     *
     * @param customerId the id for the customer to be updated
     * @param customer the customer object with the details to be updated
     * @return Response Entity object with either 'OK' and Customer object or 'NOT MODIFIED' HTTP code
     */
    @PutMapping("api/customers/{customerId}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable int customerId, @RequestBody Customer customer) {
        customer.setCustomerId(customerId);
        if (CustomerRepository.updateCustomer(customer)) {
            return ResponseEntity.status(HttpStatus.OK).body(customer);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(null);
        }
    }

    /**
     * Returns list of countries and their total customers.
     *
     * @return Response Entity object with list of countries object
     */
    @GetMapping("/api/customers/countries")
    public ResponseEntity<List<Country>> getNumberOfCustomersInCountries() {
        return ResponseEntity.status(HttpStatus.OK).body(CustomerRepository.getNumberOfCustomersInCountries());
    }

    /**
     * Returns list of highest spending customers.
     *
     * @return Response Entity object with list of spenders object
     */
    @GetMapping("/api/customers/spenders")
    public ResponseEntity<List<Spender>> getHighestSpenders() {
        return ResponseEntity.status(HttpStatus.OK).body(CustomerRepository.getHighestSpenders());
    }

    /**
     * Returns the customer object and the popular genres for the provided customer id.
     * If the provided customer id doesn't exist or customer has not purchased any tracks
     * 404 is returned.
     *
     * @return Response Entity object with list of popular genre object or 'NOT FOUND' HTTP code
     */
    @GetMapping("/api/customers/{customerId}/popular/genres")
    public ResponseEntity<PopularGenre> getCustomerPopularGenres(@PathVariable int customerId) {
        if (CustomerRepository.getCustomerPopularGenre(customerId).getCustomer() != null) {
            return ResponseEntity.status(HttpStatus.OK).body(CustomerRepository.getCustomerPopularGenre(customerId));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}

