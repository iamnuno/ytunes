package com.example.ytunes.controllers.ui;

import com.example.ytunes.models.TrackSearch;
import com.example.ytunes.queries.ArtistRepository;
import com.example.ytunes.queries.GenreRepository;
import com.example.ytunes.queries.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller that handles the index.html view.
 */
@Controller
public class IndexController {
    /**
     * Sets up index page with random artists, genres and tracks.
     * Respective calls are made to Repository classes for database queries.
     */
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("trackSearch", new TrackSearch());
        model.addAttribute("artists", ArtistRepository.getRandomArtists());
        model.addAttribute("genres", GenreRepository.getRandomGenres());
        model.addAttribute("tracks", TrackRepository.getRandomTracks());
        return "index";
    }
}
