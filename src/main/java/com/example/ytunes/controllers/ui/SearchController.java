package com.example.ytunes.controllers.ui;

import com.example.ytunes.models.TrackSearch;
import com.example.ytunes.queries.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *  * Controller that handles the search.html view.
 */
@Controller
public class SearchController {
    /**
     * Handles POST request from search box in index.html and displays track results accordingly.
     * Database queries are performed by calling the Track Repository class.
     */
    @PostMapping("/search")
    public String search(@ModelAttribute TrackSearch trackSearch, Model model) {
        model.addAttribute("searchValue", trackSearch.getSearchValue());
        model.addAttribute("tracks", TrackRepository.searchTracks(trackSearch.getSearchValue()));
        model.addAttribute("total", TrackRepository.searchTracks(trackSearch.getSearchValue()).size());
        return "search";
    }
}
